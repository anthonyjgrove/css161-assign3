
/**
 * 
 * 
 * @author Anthony Grove
 * @version CSS161 - Assignment 3
 */
import java.util.Scanner;
public class Assign3 {
    public static void main(String[] args) {
        int Height = 3;
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please enter the Height of the rocket you want us to build:");
        Height = keyboard.nextInt(); // height of rocket internal subfigures 

        displayCone(Height); // display rocket nose 
        displayLine(Height); // display line +=*=*=*=*+
        displayUpward(Height);  // display upward middle section |./\../\.|
        displayDownward(Height); // display downward middle section |.\/..\/.|
        displayLine(Height); // display line +=*=*=*=*+
        displayDownward(Height);// display downward middle section |.\/..\/.|
        displayUpward(Height); // display upward middle section |./\../\.|
        displayLine(Height); // display line +=*=*=*=*+
        displayCone(Height); // display rocket thrust 
    } 
    //prints the cone/ tail of the rocket based on the rockets height
    public static void displayCone(int x) {
        int H = ((x * 2) - 1);
        for(int count = 1; H >= count; count++) {

            for(int c1 = H; count <= c1; c1--) {
                System.out.print(" ");
            } 
            for(int c2 = 1; count >= c2; c2++) {
                System.out.print("/");
            } 
            System.out.print("**");
            for(int c2 = 1; count >= c2; c2++) {
                System.out.print("\\");
            }
            System.out.println();
        }
    }
    //prints the line of the rocket based on the rockets height
    public static void displayLine(int x) {
        int H = (x * 2);
        System.out.print("+");
        for(int count = 1; H >= count; count++) {
            System.out.print("=*");
        }
        System.out.println("+");
    }
    //prints the upward facing middle section of the rocket based on the rockets height
    public static void displayUpward(int x) {
        int H = (x);
        int D = (x - 1);
        for(int count = 1; H >= count; count++) {
            System.out.print("|");
            displayupdot(count, D);
            displayup(count, H);
            displayupdot(count, D);
            displayupdot(count, D);
            displayup(count, H);
            displayupdot(count, D);
            System.out.println("|");
        }
    }
    //method for printing upward dots
    public static void displayupdot(int z, int x) {
        int H = (x);
        for(int c1 = H; z <= c1; c1--) {
            System.out.print(".");
        }
    }
    //method for printing upward arrows
    public static void displayup(int z, int x) {
        int H = (x);
        for(int c2 = 1; z >= c2; c2++) {
            System.out.print("/\\");}

    }
    //prints the downard facing middle section of the rocket based on the rockets height
    public static void displayDownward(int x) {
        int H = (x);
        int D = (x - 1);
        for(int count = 1; H >= count; count++) {
            System.out.print("|");
            displaydowndot(count, H);
            displaydown(count, H);
            displaydowndot(count, H);
            displaydowndot(count, H);
            displaydown(count, H);
            displaydowndot(count, H);
            System.out.println("|");
        }
    }
    //method for printing downward dots
    public static void displaydowndot(int y, int x) {
        int H = (x);
        for(int c1 = 2; y >= c1; c1++) {
            System.out.print(".");
        } 
    }
    //method for printing downward arrows
    public static void displaydown(int y, int x) { 
        int H = (x);
        for(int c2 = H; y <= c2; c2--) {
            System.out.print("\\/");
        }
    }
}
